package main

import (
	"fmt"
	"bytes"
	"image/jpeg"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"strconv"

	"github.com/kbinani/screenshot"
)

func main() {
	servePort := 1234

	fmt.Println("Starting server at localhost:", strconv.Itoa(servePort))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		m := multipart.NewWriter(w)
		defer m.Close()
		w.Header().Set("Content-Type", "multipart/x-mixed-replace; boundary="+m.Boundary())
		w.Header().Set("Connection", "close")
		var (
			h = textproto.MIMEHeader{}
			b = bytes.NewBuffer(nil)
		)
		for {
			select {
			case <-r.Context().Done():
				break
			default:
				img, err := screenshot.CaptureDisplay(0)
				if err != nil {
					log.Fatal(err)
				}
				b.Reset()
				if err := jpeg.Encode(b, img, &jpeg.Options{Quality: 80}); err != nil {
					log.Fatal(err)
				}
				h.Set("Content-Type", "image/jpeg")
				h.Set("Content-Length", strconv.Itoa(b.Len()))
				mw, err := m.CreatePart(h)
				if err != nil {
					break
				}
				if _, err := mw.Write(b.Bytes()); err != nil {
					break
				}
				if f, ok := w.(http.Flusher); ok {
					f.Flush()
				}
			}
		}
	})
	http.ListenAndServe(":"+strconv.Itoa(servePort), nil)
}
